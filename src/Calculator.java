/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Cristian
 */
public class Calculator {
    
    private final String delimiter=",|\n";
    
    public int calculate(String input) throws Exception {
        String [] numbers = input.split(delimiter);
        if(isEmpty(input)){
            return 0;
        }
        
        if(numbers.length == 1){
            return stringToInt(input);
        }else{
            return getSum(numbers);
        }
    }
    private boolean isEmpty(String input){
        return input.isEmpty();
    }
    private int stringToInt(String input){
        return Integer.parseInt(input);
    }
    
    private int getSum(String [] numbers) throws Exception{
        findDangerousInput(numbers);
        return calculateValues(numbers);
    }
    private int calculateValues(String[] numbers){
         int sum=0;
        for (String current:numbers) { 
            if(stringToInt(current) > 1000){
                continue;
            }
            sum += stringToInt(current);
        }
        return sum;
    }
    private void findDangerousInput(String [] numbers) throws Exception{
        for(String current:numbers){
            if(stringToInt(current) < 0){
                throw new Exception("Negative input!");
            }
        }
    }
    
}
