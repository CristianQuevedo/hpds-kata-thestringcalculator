/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import static org.testng.Assert.*;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 *
 * @author Cristian
 */
@Test
public class CalculatorNGTest {
    private Calculator calculator;
    
    @BeforeTest
    public void init(){
        calculator= new Calculator();
    }
    
    public void emptyStringReturnsZero() throws Exception {
        assertEquals(calculator.calculate(""), 0);
    }
    
    public void sigleValueIsReplied() throws Exception{
        assertEquals(calculator.calculate("1"), 1);
    }
    
    public void twoNumbersCommaDelimitedReturnSum() throws Exception{
        assertEquals(calculator.calculate("1,2"), 3);
    }
    
    public void twoNumbersNewLineDelimitedReturnSum() throws Exception{
        assertEquals(calculator.calculate("1\n2"), 3);
    }
    
    public void threeNumbersDelimitedBothWaysReturnSum() throws Exception{
        assertEquals(calculator.calculate("1,2,3"), 6);
    }
    
    public void negativeInputReturnsException() throws Exception{
        calculator.calculate("-1");
    }
    
    public void ignoresNumbersGreaterThan1000() throws Exception{
        assertEquals(calculator.calculate("10,10,2000"), 20);
    }
}
